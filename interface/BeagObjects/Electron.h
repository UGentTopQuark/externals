#ifndef BEAG_ELECTRON_H
#define BEAG_ELECTRON_H

#include "Lepton.h"
#include "Conversion.h"
#include "Rtypes.h"

namespace beag{
	class Electron : public beag::Lepton{
		public:
			Electron():sc_eta(0), sc_phi(0), dist(0),dcot(0), passconversionveto(0),
				sigmaIetaIeta(0), deltaPhiSuperClusterTrackAtVtx(0),
				deltaEtaSuperClusterTrackAtVtx(0), hadronicOverEm(0), isEB(0){};
			virtual ~Electron(){};
			
			double sc_eta; //supercluster
			double sc_phi;
			double dist;
			double dcot;

                        int passconversionveto;

                        int nelectron; // nth electron in the initial collection, necessary for conversion assignment

			double sigmaIetaIeta;
			double deltaPhiSuperClusterTrackAtVtx;
			double deltaEtaSuperClusterTrackAtVtx;
			double hadronicOverEm;
			int isEB;

  			double myMVAVar_fbrem;
  			double myMVAVar_kfchi2;
  			double myMVAVar_kfhits; 
  			double myMVAVar_kfvalidhits; 
  			double myMVAVar_gsfchi2;  // to be checked 

  			double myMVAVar_deta;
  			double myMVAVar_dphi;
  			double myMVAVar_detacalo;
  			double myMVAVar_dphicalo;   

  			double myMVAVar_see;    //EleSigmaIEtaIEta

  			double myMVAVar_spp;
  			double myMVAVar_etawidth;
  			double myMVAVar_phiwidth;
  			double myMVAVar_e1x5e5x5;
  			double myMVAVar_R9;
  			double myMVAVar_nbrems;

  			double myMVAVar_HoE;
  			double myMVAVar_EoP;
  			double myMVAVar_IoEmIoP;  // in the future to be changed with ele.gsfTrack()->p()
  			double myMVAVar_eleEoPout;
  			double myMVAVar_EoPout;
  			double myMVAVar_PreShowerOverRaw;

  			double myMVAVar_eta;         
  			double myMVAVar_pt;       

  			double myMVAVar_d0;
			double myMVAVar_ip3d; 

		ClassDef(Electron, 1);
	};
}

#endif
