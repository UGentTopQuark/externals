#ifndef BEAG_GENPARTICLE_H
#define BEAG_GENPARTICLE_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class GenParticle : public Particle{
		public:
		GenParticle(): mother_id(0),grandmother_id(0){};
			virtual ~GenParticle(){};

			double mother_id;
			double grandmother_id;

		ClassDef(GenParticle, 1);
	};
}

#endif
