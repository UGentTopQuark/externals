#ifndef BEAGTRIGGEROBJECT_H
#define BEAGTRIGGEROBJECT_H

#include "Particle.h"
#include "Rtypes.h"

namespace beag{
	class TriggerObject: public Particle{
		public:
			TriggerObject():quality(0),is_forward(0), bx(0), detector(0){};
			virtual ~TriggerObject(){};
			
			int quality;
			int is_forward;
			double bx;
			double detector;

			std::vector<std::vector<bool> > triggered; // vector trigger-id->module->bool

		ClassDef(TriggerObject, 1);
	};
}

#endif
