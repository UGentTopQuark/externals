#ifndef BEAG_PARTICLE_H
#define BEAG_PARTICLE_H

#include "Rtypes.h"
#include "TObject.h"

namespace beag{
	class Particle: public TObject{
		public:
			Particle():eta(99),phi(99),mass(0),pt(0), mc_matched(0),from_ttbar_decay(0),charge(0),
				   mc_eta(99),mc_phi(99),mc_mass(0),mc_pt(0),mc_id(0){};
			virtual ~Particle(){};
			double eta;
			double phi;
			double mass;
			double pt;

			int mc_matched;	// flag if monte carlo truth matched

			int from_ttbar_decay;	// if particle is from ttbar decay

			char charge;

			double mc_eta;
			double mc_phi;
			double mc_mass;
			double mc_pt;
			double mc_id;		// id of monte carlo truth matched parton

		ClassDef(Particle, 1);
	};
}

#endif
