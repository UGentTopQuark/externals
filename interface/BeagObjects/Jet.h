#ifndef BEAG_JET_H
#define BEAG_JET_H

#include "Particle.h"
#include <map>
#include <string>
#include <vector>

#include "Rtypes.h"

namespace beag{
	class Jet : public Particle{
		public:
			Jet():ttbar_decay_product(0), emf(0), n90Hits(0), fHPD(0),
			nconstituents(0),chf(0),nhf(0),nemf(0),cemf(0),cmulti(0),pt_genjet(-1),
			eta_genjet(-1),phi_genjet(-1),mass_genjet(-1), jet_area(0.785398){};
			virtual ~Jet(){};

			std::vector<double> btags;	//< bDiscriminator
			/***
			 *	ttbar_decay_product:
			 *	is jet from quark from ttbar decay
			 *	possible values:
			 *	1: quark jet
			 *	2: anti-quark jet
			 *	3: hadronically decaying top b-jet
			 *	4: leptonically decaying top b-jet
			 ***/
			int ttbar_decay_product;

			double emf;	//< electromagnetic fraction
			double n90Hits;	//< minimal number of RecHits containing 90% of the jet energy
			double fHPD;	//< fraction of energy in the hottest HPD readout

			double nconstituents;	//< number of constituents
			double chf;		//< charged hardron energy fraction
			double nhf;		//< neutral hardon energy fraction
			double nemf;		//< neutral em energy fraction
			double cemf;		//< charged em energy fraction
			double cmulti;		//< charged multiplicity

			double pt_genjet;	//< pt matched gen jet
			double eta_genjet;	//< eta matched gen jet
			double phi_genjet;	//< phi matched gen jet
			double mass_genjet;	//< mass matched gen jet

			double jet_area;	//< jet area

		ClassDef(Jet, 1);
	};
}

#endif
